package ru.darya.cats;

public class Cat {
    /**
     * Кличка
     */
    private String name;

    /**
     * Пол
     */
    private String gender;

    /**
     * Год рождения
     */
    private int yearOfBirth;

    /**
     * Порода
     */
    private String breed;

    Cat(String name, String gender, int yearOfBirth, String breed) {
        this.name = name;
        this.gender = gender;
        this.yearOfBirth = yearOfBirth;
        this.breed = breed;
    }

    @Override
    public String toString() {
        return "Привет, я " + gender + " по кличке " + name + ". Моя порода: " + breed + ". Я родился(ась) в " + yearOfBirth + " году.";
    }

    void mew() {
        System.out.println(name + ": -Мяу-мяу-мяу!");
    }

    void eat() {
        System.out.println("Кажется, " + name + " голоден(а), самое время для Вискаса!");
    }

    void walk() {
        System.out.println(name + " отправляется на прогулку.");
    }
}
