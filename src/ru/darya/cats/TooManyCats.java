package ru.darya.cats;

/**
 * <b>Класс, демонстрирующий работу с объектами.</b>
 */
public class TooManyCats {
    public static void main(String args[]) {
        Cat barsik = new Cat("Барсик", "кот", 2015, "сиамская кошка");
        System.out.println(barsik);
        barsik.mew();
        Cat dymok = new Cat("Дымок", "кот", 2013, "русская голубая кошка");
        System.out.println(dymok);
        dymok.eat();
        Cat sonya = new Cat("Соня", "кошка", 2016, "персидская кошка");
        System.out.println(sonya);
        sonya.walk();
    }
}
